# fitxers-chirp

Fitxers en format CSV per a importar usant CHIRP sobre un arxiu [CHIRP](https://chirp.danplanet.com/projects/chirp/wiki/Home) obert 
(sobreescrivint el que hi ha; per exemple, carregant el binari blank.img primerament).

Estos fitxers contenen informació sobre freqüències UHF i VHF que he anat
recopilant per a viatges. No estan totes comprovades, però he usat moltes en
el meu BaoFeng UV-5RE.

Els noms son a voltes indicatius i a voltes descripcions.

Un * en el nom vol dir que cal un "burst tone" de 1750 Hz.

Un = en el nom vol dir que l'entrada ve de més d'un llistat.

Un "-U" o "-V" afegit indica UHF i VHF.
